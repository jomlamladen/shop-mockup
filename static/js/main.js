
let path = 'http://127.0.0.1:8000/';


let products = [];
let brends = [];
let orders = [];

// [
//     {
//         id:1, 
//         total: 200,
//         products: [
//             {
//                 id: 1,   
//                 title: "Iphone 11pro",   
//                 description: "lorem ipsum verde",   
//                 price: 1200,   
//                 quantity: 1,
//                 image: "https://www.placehold.it/300x300"   
//             },
//             {
//                 id: 2,   
//                 title: "Samsung s10",   
//                 description: "lorem ipsum verde",   
//                 price: 920,   
//                 quantity: 1,
//                 image: "https://www.placehold.it/300x300"   
//             }
//         ]
//     }
// ];

    /** AJAX METHODS **/
    
    /**
    * Ajax call
    * @param {str} url - api call path
    * @param {object} data - request data
    * @param {str} method  - POST, PUT, GET, DELETE, PATCH
    * @param {funct} on_success - call method after successfull request
    * @param {funct} on_error - call method after error request
    * @param {string} token - authorization token (header)
    */
   var ajaxCall 		= function(url, data, method, on_success, on_error, token) {
    let headers = {};
    if (token) {
        headers.authorization = token;
    }
    let settings =  {
        url: url,
        data: data,
        method: method,
        dataType: 'json',
        headers: headers
    };

    $.ajax(settings).done( function(res) {
        if (on_success && typeof on_success === 'function') {
            on_success(res);
        }
    }).fail( function(res) {
        if (on_error && typeof on_error === 'function') {
            on_error(res);
        }
    });
}

    let slickConfig = {
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: '<i class="arrow right">',
        prevArrow: '<i class="arrow left">',
        infinite: true,
        responsive: [
        {
            breakpoint: 1024,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            }
        },
        {
            breakpoint: 600,
            settings: {
            slidesToShow: 2,
            slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
            slidesToShow: 1,
            slidesToScroll: 1
            }
        } ]
    };

    
    let utils = {
        getForm: function(fields, form){
            let _data = {};
            $.each(fields, function(i, item){
                _data[item] = form.find('input[name="'+item+'"]').val();
            })
            return _data;
        },
        setForm: function(fields, form, data){
            $.each(fields, function(i, item){
                form.find('input[name="'+item+'"]').val(data[item]);
            })
        },
        populate: function(template, types, data){
            let res = template;
            $.each(types, function(i, type){
                res = res.split('__'+ type.toUpperCase() +'__').join(data[item]);
            });
            return res;
        },
        storageGet: function(name, parse=false){
            let data = window.localStorage.getItem(name) || null;
            return (parse === false ||data === null) ? data : JSON.parse(data);
        },
        storageSet: function(name, val, stringify=false){
            let _val = stringify === false ? val : JSON.stringify(val);
            window.localStorage.setItem(name, _val);
        },
    }

    let account = {
        token: null,
        user: null,
        authenticated: false,
        Elements:{
            login: $('.btn-login'),
            logout: $('.btn-logout'),
            registration: $('.btn-registration'),
            save: $('.btn-save'),
            loginMessage: $('.login-message'),
            regMessage: $('.registration-message'),
            saveMessage: $('.save-message'),
            checkoutMessage: $('.checkout-message'),
            checkout: $('.checkout-btn'),
            
            // TODO - dynamic model (data-type)
            buttonLogin: $('.modal-login'),
            buttonRegistration: $('.modal-registration'),
            buttonAccount: $('.modal-account'),
            buttonOrders: $('.modal-orders'),
            buttonCart: $('.cart-toggler'),
            buttonProductDelete: $('.product-delete'),
            accountForm: $('.account-form'),
            accountName: $('.account-name'),

            orderList: $('.order-list'),
            userOrders: $('.user-orders'),
            orderItem: $('.order-item'),
            orderDetails: $('.order-details'),
        },
        setToken: function(token, user){
            utils.storageSet('token', token, false);
            utils.storageSet('user', user, false);
            account.authenticated=true;
        },
        getToken: function(){
            return utils.storageGet('token', false);
        },
        getUser: function(){
            return utils.storageGet('user', false);
        },

        login: function(token, user){
            this.setToken('logovan', JSON.stringify(user));
        },
        logout: function(){
            this.setToken(null, null);
            window.location.reload();
        },
        // temporary function 
        checkUser(data){
            let that = this;
            ajaxCall(path + 'login', JSON.stringify({'email': data.email, 'password': data.password}), 'POST', function(data){
                if(data.status === 'ok'){
                    that.login('logeduser', data.data);
                    window.location.reload();
                }

            }, function(){
                that.Elements.loginMessage.html('Neuspesno logovanje');
               setTimeout(()=>{
                that.Elements.loginMessage.html('');
               }, 3000)
            })
       
        },
        updateAccountData: function(user){
            this.Elements.accountName.html('Wellcome, ' + user.name);
            utils.setForm(['id', 'name', 'email', 'address', 'city', 'phone'], this.Elements.accountForm, user);
        },
        populateOrders: function(){
            let that=this;
            let ordersHTML = ``;
            $.each(orders, function(i, order){
                ordersHTML += `<pre><h3>Order `+order.id+`</h3><div class="order-products">`;

                $.each(order.products, function(i, item){
                    ordersHTML += product.populateProduct('orderItem', item, item.quantity, true)+'<br>';
                });

                ordersHTML +=   `</div></pre>`;
            });
            
            that.Elements.orderList.html(ordersHTML);
            $('#orders').find('.loading').hide();
        },

        populateAdminOrders: function(){
            let that=this;
            let ordersHTML = ``;
            let usersHtml = `<table style="width: 100%;">`;
            $.each(orders, function(i, user){
                usersHtml += `<tr>
                <td>`+user.name+`<td><td>`+user.orders.length+`</td>
                <td>
                <div class="btn btn-primary text-center preview-user-order" data-id="`+user.id+`" data-target="#orders-user-`+user.id+`">Porudzbine</div>
                </td>
                `;
                ordersHTML += `<div style="display:none;" id="order-user-`+user.id+`" >`;
               
                $.each(user.orders, function(i, order){
                    ordersHTML += `<div style="padding-top:5px;background:#fff; border-bottom:2px solid black;"><h6>Order `+order.id+`</h6><div class="order-products">`;

                    $.each(order.products, function(i, item){
                        ordersHTML += product.populateProduct('orderItem', item, item.quantity, true)+'<br>';
                    });

                    ordersHTML +=   `</div></div>`;
                });
                
                ordersHTML + `</div></tr>`;
            });
            usersHtml += `</table>` + ordersHTML;
            
            that.Elements.orderList.html(usersHtml);
            $('#orders').find('.loading').hide();

            that.Elements.orderList.on('click', '.preview-user-order', function(){
                let data= that.Elements.orderList.find('#order-user-'+$(this).attr('data-id')).html();
                that.Elements.userOrders.html(data).show();
            });
        },
        init: function(){
            let that = this;

            this.Elements.login.on('click', function(){
                let _data = utils.getForm(['email', 'password'], $(this).parent());

                that.checkUser(_data);

            });
            
            this.Elements.logout.on('click', function(){
                that.logout();
            });
            
            this.Elements.registration.on('click', function(){
                let _data = utils.getForm(['name', 'email', 'password'], $(this).parent());
            });

            this.Elements.save.on('click', function(){
                let _data = utils.getForm(['id', 'name', 'address', 'city', 'phone'], $(this).parent());
                ajaxCall(path + 'user/'+$(this).parent().find('input[name="id"]').val(), JSON.stringify(_data), 'POST', function(data){

                    if(data.status === 'ok'){
                        that.login('logeduser', data.data);
                        that.Elements.saveMessage.html('Uspesno sacuvano');
                    }

                }, function(err){
                    console.log(err)
                    that.Elements.saveMessage.html('Neuspesno cuvanje');
                    setTimeout(()=>{
                        that.Elements.saveMessage.html('');
                    }, 3000)
                });
            });

            $('[data-call]').on('click',function(){
                // ajaxCall('', data, 'GET', function(){}, function(){});
                if($(this).attr('data-call') === 'orders'){
                    that.populateOrders()
                }else{
                    that.populateAdminOrders();
                }
            });

            if(account.getToken() === 'logovan'){

                this.Elements.buttonAccount.show();
                this.Elements.logout.show();

                this.Elements.buttonLogin.hide();
                this.Elements.buttonRegistration.hide();
                this.Elements.buttonOrders.show();

                let __user =JSON.parse(account.getUser());
                account.user = __user
                account.token = account.getToken();
                account.authenticated = true;

                if(__user.role == 1){
                    ajaxCall(path + 'orders/'+__user.id, {}, 'GET', function(data){
                        $.extend(orders, data.orders);
                    }, function(){alert('Greska')});

                    this.Elements.buttonCart.show();
                }

                if(__user.role == 2){
                    ajaxCall(path + 'orders', {}, 'GET', function(data){ $.extend(orders, data);}, function(){alert('Greska')});

                    this.Elements.buttonOrders.attr('data-call', 'adminorders');
                    this.Elements.buttonCart.hide();   
                    this.Elements.buttonProductDelete.hide();   
                }


                this.updateAccountData(__user);
            }else{
                this.Elements.buttonCart.hide();

            }
        }
        
    }

    let product = {
        Elements:{
            random: $('.product-list'),
            top: $('.top-product-list'),
            brends: $('.brend-list'),
            cartList: $('.cart-list'),
            cart: $('.product-item'),
            productDescriptionModal: $('.modal-product-description')
        },
        Data:{
            image: "https://www.placehold.it/300x300",
            imageSmall: "https://www.placehold.it/300x150"
        },
        Templates:{
            addButton: `<button data-id="__ID__" data-type="add" class="shop-now cart-action">Dodaj u korpu</button>`,
            deleteButton: `<button data-id="__ID__" data-type="deleteProduct" class="shop-now cart-action">Obrisi proizvod</button>`,
            topProductItem: `<div class="product">
                            <div class="product-top" >
                                <img class="product-image" src="__IMAGE__"  data-toggle="modal" data-target="#product-description-modal"/>
                            </div>
                            <div class="product-bottom">
                                <p class="card-text">__TITLE__</p>
                                <p class="card-text">__BREND__</p>

                                <p class="product-prices">
                                <span class="price-now">__PRICE__ €</span>
                                </p>

                                <div class="hidden product-description" style="display:none">__DESCRIPTION__</div>
                                __ADD_TO_CART__
                            </div>
                            </div>`,
            productItem: `<div class="col-md-4 product">
                            <div class="card mb-4 shadow-sm" >
                            <img class="bd-placeholder-img card-img-top" src="__IMAGE__" data-toggle="modal" data-target="#product-description-modal"/>
                            <div class="card-body">
                                <p class="card-text">__TITLE__</p>
                                <p class="card-text">__BREND__</p>
                                <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                <div type="button" class="btn btn-sm btn-outline-secondary"><span>__PRICE__</span> €</div>
                                    __ADD_TO_CART__
                                </div>
                                <div class="hidden product-description" style="display:none">__DESCRIPTION__</div>
                                </div>
                            </div>
                            </div>
                        </div>`,
            brendItem: `<li><a href="#" data-url="./telefoni.html?marka=__KEY__" class="text-white">__NAME__</a></li>`,
            cartItem: ` <div class="product" data-id="__ID__">
                            <!-- <div class="product-image">
                            <img src="__IMAGE__">
                            </div> -->
                            <div class="product-details">
                            <div class="product-title">__TITLE__</div>
                            </div>
                            <div class="product-quantity">
                            <input data-id="__ID__" data-type="update" class="cart-action quantity" type="number" value="__QUANTITY__" max="100" min="1">
                            </div>
                            
                            <!-- <div class="product-price">__PRICE__</div> -->
                            <div class="product-line-price">__PRICE__</div>
                            <div class="product-removal">
                            <button data-id="__ID__" data-type="delete" class="cart-action remove-product">
                                <span class="fa fa-remove"></span>
                            </button>
                            </div>
                        </div>`,
            orderItem: `<tr><td>__TITLE__</td><td> - __PRICE__€ - ( __QUANTITY__X )</td><td><button style="display:none;" data-id="__ID__" class="order-preview">preview</button></td></tr>`
        },
        populateProduct: function(type, item, _quantity, auth){
            let res = this.Templates[type];
            if(auth === true && account.user !== null){
                let button = (parseInt(account.user.role) == 2) ? this.Templates['deleteButton'] : this.Templates['addButton'];
                res = res.split('__ADD_TO_CART__').join(button);
            }else{
                res = res.split('__ADD_TO_CART__').join('');
            }            
            res = res.split('__IMAGE__').join('./static/image/' + item.image);
            res = res.split('__TITLE__').join(item.title);
            res = res.split('__DESCRIPTION__').join(item.description || '');
            let __brend = brends.filter((_item) => parseInt(_item.id) === parseInt(item.id_manufacture));
            res = res.split('__BREND__').join((__brend.length === 1) ? 'Proizvodjac: ' + __brend[0].name : '');
            res = res.split('__PRICE__').join(item.price);
            res = res.split('__ID__').join(item.id);
            res = res.split('__QUANTITY__').join(type == 'cartItem' ? _quantity : 1);

            return res;
        },
        
        init: function(){
            let that = this;

            let topProducts = '';
            let randomProducts = '';
            let brendList = '';
            let count_top = 3;
            $.each(products, function(i, item){
                if(i <= count_top){
                    topProducts += that.populateProduct('topProductItem', item, null, account.authenticated);
                }
                randomProducts += that.populateProduct('productItem', item, null, account.authenticated);
            });
    
            $.each(brends, function(i, item){
                let tmp = that.Templates.brendItem;
                tmp = tmp.split('__KEY__').join(item.key);
                tmp = tmp.split('__NAME__').join(item.name)
                brendList +=  tmp;
            })

            that.Elements.brends.html(brendList);
            that.Elements.top.html(topProducts);
            that.Elements.random.html(randomProducts);
            $('.product-carousel').slick(slickConfig);

            $(document).on('click', '.product', function(){
                
                that.Elements.productDescriptionModal.html($(this).find('.product-description').html());
            });


        },
    } 
    
    let shop = {

        
        // Defined elements 
        Elements: {
            action: $('.cart-action'),    
            preview: $('.cart-preview'),
            content: $('.shopping-cart-box'),
            priceTotal: $('.cart-total-price') ,
            totalInCart: $('.cart-total-items')
        },
        Data:{
            item: {
                id: null,
                quantity: 1
            },
            cart: [],
        },
        fn:{
            _action: '.cart-action',

            populateList: function(products = null){
                $.each(products || shop.Data.cart, (i, item) => {
                    this.populateItem(item.product, item.quantity);
                });
                this.updatePrice();

            },
            populateItem: function(_product, _quantity=1){

                let item = product.populateProduct('cartItem', _product, _quantity=1, true);
                product.Elements.cartList.append(item)
            },
            updatePrice: function(){
                let total = 0;
                let totalInCart = 0;
                $.each(shop.Data.cart, (i, item) => {
                    totalInCart += item.quantity;
                    total = total + (item.quantity * item.product.price);
                    $('.cart-list').find('.product[data-id="'+item.id+'"]').find('input.quantity').val(item.quantity)
                });
               shop.Elements.priceTotal.html(total);
               shop.Elements.totalInCart.html(totalInCart)
               shop.Data.total = total;
                //Update localstorage cart
               utils.storageSet('cart', shop.Data.cart, true);
            },

            open: function(){
                shop.Elements.content.toggleClass('active');
            },
            // Add product to cart function
            add: function(that){
                let _id = parseInt($(that).attr('data-id'));
                let _product = products.filter((item) => parseInt(item.id) === _id);
                let _inOrder = shop.Data.cart.filter((item) => parseInt(item.id) === _id);
         
                
                if(_product.length > 0 && _inOrder.length === 0){

                    shop.Data.cart.push({
                        id: _id,
                        quantity: 1,
                        product: _product[0],
                    });

                    this.populateItem(_product[0])
                    
                    this.updatePrice();

                }else if(_product.length > 0 && _inOrder.length > 0){
                    _inOrder[0].quantity +=1;
                    this.updatePrice();
                }else{
                    alert('Nepostojeci proizvod')
                }
            },
            
            // Add product to cart function
            update: function(that){
                let _id = parseInt($(that).attr('data-id'));
                let _product = shop.Data.cart.filter((item) => item.id === _id);

                if(_product.length > 0){
                    _product[0].quantity = parseInt($(that).val());
                }
                this.updatePrice();
            },
            
            // Remove product from cart and update total price
            delete: function(that){
                let _id = parseInt($(that).attr('data-id'));
                shop.Data.cart.splice(shop.Data.cart.findIndex(item => item.id === _id), 1);
                product.Elements.cartList.find('.product[data-id="'+ _id +'"]').remove();
                this.updatePrice();
            },

            deleteAll: function(that){
                shop.Data.cart=[];
                product.Elements.cartList.find('.product').remove();
                this.updatePrice();
            },

            deleteProduct: function(that){
                if(confirm('Obrisi ovaj proizvod')){
                    alert('Uspesno ste obrisali proizvod (test)');


                    // ajaxCall(path + 'product', JSON.stringify({"id_product": $(that).attr('data-id'), "id_user": account.user.id}), 'DELETE', function(data){

                    //     if(data.status === 'ok'){
                    //         alert('Uspesno ste obrisali proizvod');
                    //         that.delete(that);
                    //         setTimeout(()=>{
                    //             window.location.reload();
                    //         },2000);
                    //     }
                    // }, function(){});
                }
            },
        
            // Add product to cart function
            checkout: function(){
                let that = this;
                if(confirm('Kupi ove proizvode')){
                    
                    let _products = [];
                    $.each(shop.Data.cart, function(i, item){
                        _products.push({
                            id: item.id,
                            price: item.product.price,
                            quantity: item.quantity
                        })
                    });
                    
                    let _data = {
                        id_user: account.user.id,
                        total: shop.Data.total,
                        products: _products
                    }

                    ajaxCall(path + 'order', JSON.stringify(_data), 'POST', function(data){

                        if(data.status === 'ok'){
                            alert('Uspesno ste izvrsili kupovinu');
                            that.deleteAll();
                            setTimeout(()=>{
                                window.location.reload();
                            },2000);
                        }
                        

                        // RESPONSE
                        // {
                        //     "id_user": 1,
                        //     "total":  900,
                        //     "products": [
                        //         {
                        //             "id": 1,
                        //             "price": 900,
                        //             "quantity": 1
                        //         }
                        //     ]
                        // }

                    }, function(err){
                        console.log(err)
                        that.Elements.checkoutMessage.html('Neuspesno cuvanje');
                        setTimeout(()=>{
                            that.Elements.checkoutMessage.html('');
                        }, 3000)
                    });
                }
            },
            
            // Handle click event
            handler: function(){
                shop.fn[$(this).attr('data-type')](this);
            },
            
            // Attach available events
            attachedEvents: function(){
                $(document).on('click change', this._action, this.handler);
            }
        },
        init:function(){
            this.fn.attachedEvents();
            
            if(utils.storageGet('cart', true) !== null){
                $.extend(shop.Data.cart, utils.storageGet('cart', true));
                shop.fn.populateList();

            }
            
        }
    }


