<?php
namespace backend\Controller;

class Base {
    
    protected function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    protected function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        $response['status'] = 'error';
        return $response;
    }

    protected function unauthorizedResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 301 Unatuhorized request';
        $response['body'] = json_encode([
            'error' => 'Unathorized request',
            'status' => 'error'
        ]);
        return $response;
    }
    
    protected function wrongCredentialsReposnse()
    {
        $response['status_code_header'] = 'HTTP/1.1 400';
        $response['body'] = json_encode([
            'error' => 'Wrong credentials',
            'status' => 'error'
        ]);
        return $response;
    }

}