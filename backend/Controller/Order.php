<?php
namespace backend\Controller;
require_once('./Controller/Base.php');
require_once('./Model/_User.php');

use backend\Model\_User;

class Order extends Base{

    private $db;
    private $requestMethod;
    private $id;

    private $user;

    public function __construct($db, $requestMethod, $id)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->id = $id;

        $this->user = new _User($db);
    }
    
     // {
    //     "id_user": 1,
    //     "total":  900,
    //     "products": [
    //         {
    //             "id": 1,
    //             "price": 900,
    //             "quantity": 1
    //         }
    //     ]
    // }
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if($this->id != null){
                    $orders = $this->getOrders($this->id);
                    $response = [];
                    $response['status_code_header'] = 'HTTP/1.1 200 OK';
                    $response['body'] = json_encode([
                        "orders" => $orders,
                        "status"=> "ok"
                        ]);
                    
                }else{
                    $response = $this->getUsersWithOrders();
                }
                break;
            case 'POST':
                $response = $this->addOrder();
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }


    
    public function addOrder(){
        
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if(!key_exists('id_user', $input) && !key_exists('total', $input) && !key_exists('products', $input)){
            $response = $this->unprocessableEntityResponse();
        }else{

            $result = $this->user->addOrder($input['id_user'], $input['products'], $input['total']);
            if(!key_exists('id_order', $result)){
                $response = $this->wrongCredentialsReposnse(); 
            }else{

                $response = [];
                $response['status_code_header'] = 'HTTP/1.1 200 OK';
                $response['body'] = json_encode([
                    "data" => $result,
                    "status" => "ok"
                ]);
            }    
        }

        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }
    private function getOrders($id){
        $orders = $this->user->orders($id);
            
        foreach($orders as $key => $order){
            $products = $this->user->orderProducts($order['id']);
            if(!$products){
                $orders[$key]['products'] = [];
            }else{
                $orders[$key]['products'] = $products;
            }

        }

       return $orders;
    }

    private function getUsersWithOrders()
    {
        $result = $this->user->findAll();
        
        if (! $result) {
            return $this->notFoundResponse();
        }

        foreach ($result as $key =>  $user) {
            $result[$key]['orders'] = $this->getOrders($user['id']);
        }

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }



}