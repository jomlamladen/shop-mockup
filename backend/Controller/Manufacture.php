<?php
namespace backend\Controller;
require_once('./Controller/Base.php');
require_once('./Model/_User.php');
require_once('./Model/_Manufacture.php');

use backend\Model\_User;
use backend\Model\_Manufacture;
use backend\Controller\Base;

class Manufacture extends Base {

    private $db;
    private $requestMethod;

    private $manufacture;

    public function __construct($db, $requestMethod)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;

        $this->manufacture = new _Manufacture($db);
    }
    

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                    $response = $this->getAllManufactures();
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllManufactures()
    {
        $result = $this->manufacture->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    
}