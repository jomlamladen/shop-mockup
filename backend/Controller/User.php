<?php
namespace backend\Controller;
require_once('./Controller/Base.php');
require_once('./Model/_User.php');

use backend\Model\_User;

class User extends Base{

    private $db;
    private $requestMethod;
    private $userId;

    private $user;

    public function __construct($db, $requestMethod, $userId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->userId = $userId;

        $this->user = new _User($db);
    }
    

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->userId) {
                    $response = $this->getUser($this->userId);
                } else {
                    $response = $this->getAllUsers();
                };
                break;
            // case 'POST':
            //     $response = $this->createUserFromRequest();
            //     break;
            case 'POST':
                $response = $this->updateUserFromRequest($this->userId);
                break;
            case 'DELETE':
                $response = $this->deleteUser($this->userId);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllUsers()
    {
        $result = $this->user->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    /**
        * 
        * 
        * Response
        * {
        * "data": {
        *    "id": "1",
        *    "email": "admin@mailinator.com",
        *    "name": "Petar Stevic",
        *    "address": "Gandijeva 5",
        *    "city": "Beograd",
        *    "role": "1"
        *},
        *"status": "ok"
        *}
        * 
        */
    public function login(){
        
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if(!key_exists('email', $input) && !key_exists('password', $input)){
            $response = $this->unprocessableEntityResponse();
        }else{

            $result = $this->user->findByEmailAndPassword($input['email'], $input['password']);
            if(!$result){
                $response = $this->wrongCredentialsReposnse(); 
            }else{

                $result['orders'] = $this->getOrders($result['id']);

                $response = [];
                $response['status_code_header'] = 'HTTP/1.1 200 OK';
                $response['body'] = json_encode([
                    "data" => $result,
                    "status" => "ok"
                ]);
            }    
        }

        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }
    private function getOrders($id){
        $orders = $this->user->orders($id);
        if($orders){
            
            foreach($orders as $order){
                $products = $this->user->orderProducts($order['id']);
                if(!$products){
                    $order['products'] = [];
                }else{
                    $order['products'] = $products;
                }

            }
        }else{
            $orders = [];
        }

       return $orders;
    }
    private function getUser($id)
    {
        $result = $this->user->find($id);
        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $result['orders'] = $this->getOrders($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createUserFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateUser($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->user->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function updateUserFromRequest($id)
    {
        $result = $this->user->find($id);
        if (!$result) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        $this->user->update($id, $input);

        $user = $this->user->find($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode(["status" => "ok", "data"=>$user, "message"=> "Successfully updated"]);
        return $response;
    }

    private function deleteUser($id)
    {
        $result = $this->user->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->user->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function validateUser($input){
        $fields = ["email", "name"];
    
        foreach ($fields as $value) {
            if (!isset($input[$value])) {
                return false;
            }
        }
    
        return true;
    }

}