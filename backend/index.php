<?php
require_once('./Controller/User.php');
require_once('./Controller/Manufacture.php');
require_once('./Controller/Product.php');
require_once('./Controller/Order.php');
require_once('db.php');

use backend\Controller\User;
use backend\Controller\Manufacture;
use backend\Controller\Product;
use backend\Controller\Order;
use backend\DB;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PATCH,DELETE");
header("Access-Control-Max-Age: 3600");
// header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode('/', $uri);

// all of our endpoints start with 
// everything else results in a 404 Not Found
if (!in_array($uri[1], ['login', 'user', 'users', 'product', 'products', 'manufactures', 'order', 'orders'])) {
    header("HTTP/1.1 404 Not Found");
    exit();
}

$path = $uri[1];
$id = null;
if (isset($uri[2])) {
    $id = (int) $uri[2];
}


// authenticate the request with Okta:
// if (! authenticate()) {
//     header("HTTP/1.1 401 Unauthorized");
//     exit('Unauthorized');
// }



define("DB_HOST", "localhost");
// define("DB_PORT", "");
define("DB_DATABASE", "dipl");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "123123123");

$dbConnection = (new DB())->getConnection();
$requestMethod = $_SERVER["REQUEST_METHOD"];


switch ($path) {

    case 'login':
        $controller = new User($dbConnection, $requestMethod, null);
        $controller->login();
    break;

    case 'user':
    case 'users':

        $controller = new User($dbConnection, $requestMethod, $id);
        $controller->processRequest();

        break;
    case 'product':
    case 'products':

        $controller = new Product($dbConnection, $requestMethod, $id);
        $controller->processRequest();

    break;
    case 'manufactures':
        
        $controller = new Manufacture($dbConnection, $requestMethod);
        $controller->processRequest();
    break;
        
    case 'orders':
    case 'order':
            $controller = new Order($dbConnection, $requestMethod, $id);
            $controller->processRequest();

    break;
    
    // default:
    //     header("HTTP/1.1 404");
    //     exit('Not found');
    //     break;
}
