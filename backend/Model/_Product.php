<?php
namespace Backend\Model;

class _Product {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
        $this->model = 'products';
    }
    
    public function findAll()
    {
        $statement = "
            SELECT
                id, title, description, price, code, image, id_user, id_manufacture
            FROM
                {$this->model};
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($id)
    {
        $statement = "
            SELECT
                id, title, description, price, code, image, id_user, id_manufacture
            FROM
                {$this->model}
            WHERE id = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(Array $input)
    {
        $statement = "
            INSERT INTO {$this->model}

                (title, description, price, code)
            VALUES
                (:title, :description, :price, :code, :image);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'title' => $input['title'],
                'description'  => $input['description'],
                'price' => $input['price'] ?? null,
                'code' => $input['code'] ?? null,
                'image' => $input['image'] ?? 1,
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function update($id, Array $input)
    {   
        $data = array(
            'id' => (int) $id
        );

        $fields = ['title', 'description', 'price', 'image'];

        $statement = "UPDATE {$this->model} SET ";
            
        for($i = 0; $i<count($fields); $i++){
            
            if(isset($input[$fields[$i]]) && strlen($input[$fields[$i]]) > 2){
                $statement .= $fields[$i]."= :".$fields[$i];

                $data[$fields[$i]] = $input[$fields[$i]];
            }
        }
        
        $statement .= " WHERE id = :id; ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute($data);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function delete($id)
    {
        $statement = "
            DELETE FROM {$this->model}
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array('id' => $id));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}