<?php
namespace Backend\Model;

class _Manufacture {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
        $this->model = 'manufactures';
    }
    
    public function findAll()
    {
        $statement = "
            SELECT
                id, slug, name
            FROM
                {$this->model};
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    
}