<?php
namespace Backend\Model;

class _User {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
        $this->model = 'users';
    }
    
    public function findAll()
    {
        $statement = "
            SELECT
                id, email, name, address, city, role
            FROM
                {$this->model};
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($id)
    {
        $statement = "
            SELECT
                id, email, name, address, city, role, phone
            FROM
                {$this->model}
            WHERE id = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function addOrder($id_user, Array $products, $total){
        $statement = "
            INSERT INTO orders
                (id_user, total)
            VALUES
                (:id_user, :total);
            ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'id_user' => $id_user,
                'total'  => $total
            ));
            // $order = $statement->rowCount();
            $order_id = $this->db->lastInsertId();

        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
        $statement = "INSERT INTO order_2_products (id_product, id_order, quantity, total) VALUES";
        $i = 0;
        foreach ($products as $product) {
            
            $statement .="(".$product['id'].", ".$order_id.", ".$product['quantity'].", ". ($product['price'] * $product['quantity']) . ")";
            
            if($i < count($products)-1){
                $statement .=",";
            }
            
            $i++;
        }

        $statement .=";";

        try {
            $statement = $this->db->exec($statement);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        return ["id_order" => $order_id];
        
    }
    public function orders($id)
    {
        $statement = "
            SELECT
                id, id_user, total
            FROM
                orders
            WHERE id_user = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    public function orderProducts($id)
    {
        $statement = "
            SELECT
                p.id, p.title, p.price, p.id_manufacture, o.quantity 
            FROM
                order_2_products o 
            JOIN products p
            WHERE o.id_order = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }


    
    public function findByEmailAndPassword($email, $pass)
    {
        $statement = "
            SELECT
                id, email, name, address, city, role, phone
            FROM
                {$this->model}
            WHERE email = ? and password = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($email, $pass));
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(Array $input)
    {
        $statement = "
            INSERT INTO {$this->model}

                (email, name, address, city, role)
            VALUES
                (:email, :name, :address, :city, :role);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'emial' => $input['email'],
                'name'  => $input['name'],
                'address' => $input['address'] ?? null,
                'city' => $input['city'] ?? null,
                'role' => $input['role'] ?? 1,
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function update($id, Array $input)
    {   
        $data = array(
            'id' => (int) $id
        );

        $fields = ['name', 'address', 'city', 'phone'];

        $statement = "UPDATE {$this->model} SET ";
            
        for($i = 0; $i<count($fields); $i++){
            
            if(isset($input[$fields[$i]]) && strlen($input[$fields[$i]]) > 2){
                $statement .= $fields[$i]." = :".$fields[$i];
                if($i < count($fields)-1){
                    $statement .=", ";
                }
                $data[$fields[$i]] = $input[$fields[$i]];
            }
        }
        
        $statement .= " WHERE id = :id; ";
       
        try {
            $statement = $this->db->prepare($statement);
            $statement->execute($data);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function delete($id)
    {
        $statement = "
            DELETE FROM {$this->model}
            WHERE id = :id AND role != 2;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array('id' => $id));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}