

--
-- Table structure for table `roles`
--


CREATE TABLE IF NOT EXISTS `roles` (
	`id` int(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`name` varchar(60) NOT NULL
);

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'USER'),(2, 'ADMIN');


--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `email` varchar(255) UNIQUE NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255),
  `phone` varchar(255),
  `role` int(2) default 1
);


INSERT INTO `users` (`id`, `email`, `name`, `password`, `address`, `city`) VALUES
(1, 'admin@mailinator.com','Petar Stevic', '123123123', 'Gandijeva 5', 'Beograd'),
(2, 'user@mailinator.com','Nikola Nikolic', '123123123', 'Gandijeva 15', 'Beograd'),
(3, 'user2@mailinator.com','Stefan Peric', '123123123', 'Gandijeva 22', 'Beograd');



--
-- Table structure for table `manufactures`
--
CREATE TABLE IF NOT EXISTS `manufactures` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(60) NOT NULL,
  `slug` VARCHAR(60) NOT NULL,
  `image` VARCHAR(60),
  `description` Text
);

INSERT INTO `manufactures` (`id`, `name`, `slug`) VALUES
(1, 'Apple', 'apple'), (2, 'Samsung', 'samsung'), (3, 'Huawei', 'huawei'),(4, 'Honor', 'honor');

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `image` varchar(255) NOT NULL,
  `id_user` int(6),
  FOREIGN KEY (`id_user`) REFERENCES users(`id`),
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
);


--
-- Dumping data for table `products`
--

INSERT INTO `products` (`title`, `description`, `code`, `price`, `image`, `id_user`) VALUES ('iPhone 11 pro', 'Pro kamere. Pro ekran. Pro performanse. <br> Tri kamere. Bezbroj mogućnosti. <br> Utrostručite vaše veštine sa kamerom.<br>Najnapredniji iPhone do sad.<br>Nadogradite se sa najboljnim iPhoneom do sad.<br>Snimajte neverovatne video snimke i fotografije sa Ultra Wide, Wide i Telefoto kamerama. Slikajte vaše najbolje slike niske osvetljenosti sa novim Night mode-om. Gledajte HDR filmove i emisije na 5.8-inčnom Super Retina XDR ekranu - najsvetlijem iPhone ekranu do sad.<br>Iskusite neviđene performanse sa A13 Bionic čipom, za igrice, proširenu realnost (AR) i fotografiju, kao i celodnevno trajanje baterije i nov nivo otpornosti na vodu.<br>Sve to u prvom iPhoneu dovoljno snažnom da se nazove Pro.', '0i11p', 600, 'product-images/laptop.jpg', 1);
INSERT INTO `products` (`title`, `description`, `code`, `price`, `image`, `id_user`) VALUES ('iPhone XS Max', 'Super Retina u dve veličine - uključujući najveći ekran ikada na iPhone-u. Čak i brži Face ID. Najpametniji, najmoćniji čip na pametnom telefonu. Probojni sistem dvostrukog fotoaparata. iPhone Xs i iPhone Xs Max su sve što volite u iPhone-u. Za sve krajnosti.<br>Super Retina u dve veličine, uključujući najveći ekran ikad viđen na iPhone-u. Još brži Face ID. Najpametniji i najmoćniji čip za pametne telefone. I revolucionarni sistem sa dvojnom kamerom. iPhone XS ima sve što voliš kod iPhone-a. Samo lepše, brže i bolje.<br>Super Retina. Super velika. OLED ekrani napravljeni upravo za iPhone XS prikazuju sliku HDR kvaliteta sa dubokim nijansama crne i najvernijim prikazom boja među pametnim telefonima. A iPhone Xs Max se može pohvaliti sa najvećim ekranom u istoriji iPhone-a.', '00ixs', 50, 'product-images/laptop-bag.jpg', 1);
INSERT INTO `products` (`title`, `description`, `code`, `price`, `image`, `id_user`) VALUES ('iPhone X', 'Specifikacije<br><br>iOS 11 na početku prodaje<br>Interna memorija 64 i 256 GB<br>Ekran 5.8 inča, AMOLED, 2436x1125 piksela, 458 ppi, TrueTone, automatska kontrola osvetljenosti, odnos kontrasta: 1,000,000: 1, 3D Touch, maksimalna osvetljenost u manuelnom režimu 625 nita<br>Procesor A11 Bionic 64 bitni, ugrađeni koprocesor M11<br>3 GB RAM-a<br>IP67 zaštita protiv vode i prašine<br>Kućište od nerđajućeg čelika<br>Baterija Li-Ion 2716 mAh, brzo punjenje (potreban vam je zasebni adapter), bežično punjenje Qi, vreme rada pri surfovanju internetom 13 sati, u režimu gledanja video sadržaja - do 14 sati<br>nanoSIM<br>Prednja kamera 7 MP, 1080p video snimanje, f / 2.2, BSI, serijsko snimanje, HDR, pozadinsko osvetljnje umesto blica<br>Glavna kamera je dvostruka, 12 MP, širokog ugla (f / 1.8) i telefoto sočiva (f / 2.4), portrait režim, stage lighting (beta verzija), optički zum x2, blic sa korekcijom nijanse, optička stabilizacija za dva sočiva , HDR, kontinualno snimanje, JPEG i HEIF format slike<br>Video snimanje 4K pri 60 kadrova u sekundi, usporeni video 240 kadrova u sekundi za FullHD<br>H.264 i HEVC video format<br>FaceID - Detektovanje lica<br>Senzori - barometar, troosni žiroskop, acelerometar, senzor blizine, senzor ambijentlanog osvetljenja<br>Stereo zvučnicci - drugi zvučnik je konverzacioni<br>Apple Pay<br>NFC (samo za Apple Pay)<br>Wi-Fi 802.11 b / n / g / ac, dual-band, Bluetooth 5.0<br>Digitalni kompas<br>GPS / GLONASS<br>Dimenzije - 143.6x70.9x7.7 mm, masa - 174 grama', '000ix', 700, 'product-images/iphone.jpg', 1);

INSERT INTO `products` (`title`, `description`, `code`, `price`, `image`, `id_user`) VALUES ('Samsung S20 Ultra', 'Uz veći senzor na Galaxy S20 seriji, rezolucija kamere je značajno povećana, što rezultira u više detalja i dodatnoj fleksibilnosti za uređivanje, isecanje i zumiranje snimaka. Samsung Galaxy S20 Ultra dolazi sa fantastičnom kamerom od 108 MP koja koristi još više svetlosti, što donosi bolji kvalitet slike čak i u uslovima slabog osvetljenja. Uz Space Zoom tehnologiju koja kombinacije opcije Hybrid Optic Zoom i Super Resolution Zoom, uključujući digitalni zoom na bazi veštačke inteligencije, možeš uveličati objekat čak i do 100 puta.', '000s20', 950, 'product-images/samsungultra.jpg', 1);
INSERT INTO `products` (`title`, `description`, `code`, `price`, `image`, `id_user`) VALUES ('Samsung S10', 'Linija Galaxy u 2019. godini slavi deset godina postojanja, predstavljena je deseta generacija pametnih telefona ove linije. U pitanju su najpopularniji pametni telefoni bazirani na Androidu, njihova prodaja meri se stotinama miliona jedinica tokom prethodnih deset godina. Samsung je oduvek nastojao da pravi beskompromisne uređaje, koji bi imali najnoviju tehnologiju, a fokusirali su se ne samo na samu tehnologiju, već i na jednostavnost upotrebe. Ove godine, tri modela se pojavljuju na tržištu istovremeno, a ne dva, kao što smo navikli. Obično kompanija predstavi dva modela: jedan manji i drugi, sa prefiksom “plus”, veći. Ali njihov dizajn i mogućnosti su uvek bili identični. Sa S10 i S10 +, ova tradicija se nastavlja, ali se sada pojavljuje još jedan mlađi model, koji se razlikuje po ravnom ekranu, to je ono što su mnogi kupci tražili. Pored ravnog ekrana, on je i model najslabijih karakteristika, a samim tim i njegova cena je najniža od sva tri. Razmotrićemo ovaj uređaj u posebnom materijalu, ali za sada želimo da se fokusiramo na dva starija modela, jer će mnogi među njima izabrati uređaj za sebe.', '000s10', 420, 'product-images/samsungs10.jpg', 1);
INSERT INTO `products` (`title`, `description`, `code`, `price`, `image`, `id_user`) VALUES ('Honor One Plus', 'Ekran: 6,26" FullView ekran <br>Memorija: RAM: 8 GB / ROM: 256 GB <br>Kamera: Zadnja: 48 MP / Prednja: 32 MP <br>Procesor: Kirin 980 AI čipset sa Dual-NPU <br>Baterija: 4000 mAh', '000one', 640, 'product-images/oneplus.jpg', 1);
INSERT INTO `products` (`title`, `description`, `code`, `price`, `image`, `id_user`) VALUES ('Huawei P40 Pro', 'Huawei P40 Pro', '00040', 840, 'product-images/p40pro.jpg', 1);



CREATE TABLE IF NOT EXISTS `orders` (
  id INT(6) AUTO_INCREMENT PRIMARY KEY,
  id_user INT(6),
  FOREIGN KEY (id_user) REFERENCES users(id),
  total FLOAT default 0,
  quantity INT(6) default 1

);


CREATE TABLE IF NOT EXISTS `order_2_products` (
  id_product INT(10),
  id_order INT(6),
  quantity INT(6) default 1,
  total FLOAT default 0,
  FOREIGN KEY (id_product) REFERENCES products(`id`),
  FOREIGN KEY (id_order) REFERENCES orders(`id`)
);


